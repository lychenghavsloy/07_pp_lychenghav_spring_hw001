package com.example.spring_hw1;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
public class Controller {

    ArrayList<Customer> list = new ArrayList<>();

    public Controller() {
        list.add(new Customer(1, "Chenghav", "male", 21, "PhnomPenh"));
        list.add(new Customer(2, "Rithysak", "male", 21, "BattamBong"));
        list.add(new Customer(3, "Vannak", "male", 21, "Kandal"));
    }

    @PostMapping("/api/v1/customers")
    public ResponseEntity<?> insertCustomer(@RequestBody Customer lists) {
        list.add(lists);
        return ResponseEntity.ok((
                new messageResponse<ArrayList<Customer>>(
                        LocalDateTime.now(), HttpStatus.OK, "This record was successfully created", list
                )
                ));
    }

    @GetMapping("/api/v1/cutomers/{customerId}")
    public ResponseEntity<?> readCustomerById(@PathVariable Integer customerId) {
        for (Customer cs : list) {
            if (cs.getId() == customerId) {
                return ResponseEntity.ok((
                        new messageResponse<Customer>(
                                LocalDateTime.now(), HttpStatus.OK, "The record has found successfully", cs
                        )
                        ));
            }
        }
        return null;
    }

    @GetMapping("/api/v1/customers")
    public ResponseEntity<?> getAllCustomer() {
        return ResponseEntity.ok((
                new messageResponse<ArrayList<Customer>>(
                        LocalDateTime.now(), HttpStatus.OK, "The record has found successfully", list
                )
        )
        );
    }

    @GetMapping("/api/v1/customers/search")
    public ResponseEntity<?> readCustomerByName(@RequestParam String search) {
        for (Customer sea : list) {
            if (sea.getName().equalsIgnoreCase(search)) {
                return ResponseEntity.ok((
                        new messageResponse<Customer>(
                                LocalDateTime.now(), HttpStatus.OK, "This record has found successfully", sea
                        )
                        ));
            }
        }
        return null;
    }

    @PutMapping("/api/v1/customers/{customerId}")
    public ResponseEntity<?> updateCustomerById(@PathVariable("customerId") Integer customerId, @RequestBody Customer cus) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId() == customerId) {
                list.set(i, new Customer(customerId, cus.getName(), cus.getGender(), cus.getAge(), cus.getAddress()));
                return ResponseEntity.ok((
                        new messageResponse<Customer>(
                                LocalDateTime.now(), HttpStatus.OK, "Your Update successfully", cus
                        )

                        ));
            }
        }
        return null;
    }

    @DeleteMapping("/api/v1/customers/{customerId}")
    public ResponseEntity<?> deleteCustomerById(@PathVariable("customerId") Integer customerId) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId() == customerId) {
                list.remove(i);
                return ResponseEntity.ok((
                        new messageResponse<ArrayList<Customer>>(
                                LocalDateTime.now(), HttpStatus.OK, "Congratulation your delete is successfully", list
                        )
                        ));
            }
        }
        return ResponseEntity.ok((
                new messageResponse<ArrayList<Customer>>(
                        LocalDateTime.now(), HttpStatus.NOT_FOUND, "The save with update not found id",list
                )
                ));
    }
}
