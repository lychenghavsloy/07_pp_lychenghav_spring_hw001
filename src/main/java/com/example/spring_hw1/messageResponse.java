package com.example.spring_hw1;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.time.LocalTime;

public class messageResponse <T>{


    private String message;

    private T payload;
    private HttpStatus status;
    private LocalDateTime dateTime;


    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }

    public messageResponse(LocalDateTime dateTime, HttpStatus status, String message, T payload) {
        this.dateTime = dateTime;
        this.status = status;
        this.message = message;
        this.payload = payload;





    }
}
